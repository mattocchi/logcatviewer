package it.aleven.logcatviewer;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.comparator.LastModifiedFileComparator;

/**
 * Servlet implementation class View
 */
public class View extends HttpServlet {

	private static final int BUFSIZE = 1024;
	private static final long serialVersionUID = 1L;
	private static final String ENCODING_UTF8 = "UTF-8";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public View() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGetOrPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGetOrPost(request, response);
	}

	private void doGetOrPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String currentPath = getServletContext().getRealPath("/");

		String value = request.getParameter("name");

		// // response.getOutputStream().print(absoluteDiskPath);
		// File f = FileUtils.getFile(absoluteDiskPath);
		//
		//

		File f = new File(currentPath);

		File tomcat = f.getParentFile().getParentFile();
		File logsDir = new File(tomcat.getAbsolutePath(), "logs");

		if (logsDir.exists()) {

			if (value != null && !value.isEmpty()) {

				/*
				 * CONTENUTO DEL FILE
				 */

				File logFile = new File(logsDir.getAbsoluteFile(), value); // "atreeflow.log"

				if (logFile.exists()) {

					String logFilePath = logFile.getAbsolutePath();

					File file = new File(logFilePath);
					int length = 0;
					ServletOutputStream outStream = response.getOutputStream();
					ServletContext context = getServletConfig().getServletContext();
					String mimetype = context.getMimeType(logFilePath);

					response.setCharacterEncoding(ENCODING_UTF8);

					// sets response content type
					if (mimetype == null) {
						// mimetype = "application/octet-stream";
						mimetype = "text/plain";
					}
					response.setContentType(mimetype);
					response.setContentLength((int) file.length());
					String fileName = (new File(logFilePath)).getName();

					// sets HTTP header
					// response.setHeader("Content-Disposition",
					// "attachment; filename=\"" + fileName + "\"");
					response.setHeader("Content-Disposition", "inline; filename=\"" + fileName + "\"");

					byte[] byteBuffer = new byte[BUFSIZE];
					DataInputStream in = new DataInputStream(new FileInputStream(file));

					// reads the file's bytes and writes them to the response
					// stream
					while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
						outStream.write(byteBuffer, 0, length);
					}

					in.close();
					outStream.close();
				}
			} else {

				/*
				 * ELENCO DEI FILES NELLA DIRECTORY
				 */
				response.setContentType("text/html");

				PrintWriter out = response.getWriter();
				out.println("<html>");
				out.println("<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset="+ENCODING_UTF8+"\"></head>");
				out.println("<body>");
				out.println("<ul>");

				File[] files = logsDir.listFiles();
				Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);

				for (final File fileEntry : files) {
					if (fileEntry.isDirectory()) {
						// listFilesForFolder(fileEntry);
					} else {
						out.println("<li>");
						// System.out.println(fileEntry.getName());
						out.println("<a href=\"?name=" + fileEntry.getName() + "\" title=\"" + new Date(fileEntry.lastModified()) + "\">" + fileEntry.getName() + "</a>");
						out.println("</li>");
					}
				}

				out.println("</ul>");
				out.println("</body>");
				out.println("</html>");
				out.close();
			}

		}
	}
}
